#!/bin/bash

# Code modified from https://github.com/building5/ansible-vault-tools
# Originally distributed under ISC License:
#     Copyright (c) 2016, David M. Lee, II <leedm777@yahoo.com>
#
#     Permission to use, copy, modify, and/or distribute this software for any
#     purpose with or without fee is hereby granted, provided that the above
#     copyright notice and this permission notice appear in all copies.
#
#     THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
#     WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
#     MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
#     ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
#     WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
#     ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
#     OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

#
# ansible-vault-merge: helper script for merging changes in ansible-vault file
#

PROGNAME=$(basename $0)

usage() {
    cat <<EOF
usage: ${PROGNAME} [OPTION]... [--] BASE CURRENT OTHER [LOCATION]

  -h, --help Display this help
EOF
}

while test $# -gt 0; do
    case $1 in
        --help|-h)
            usage
            exit 0
            ;;
        --)
            shift 1
            break
            ;;
        -*)
            echo "${PROGNAME}: unknown option $1" >&2
            usage >&2
            exit 1
            ;;
        *)
            # probably the first positional argument
            break
    esac
done

if test $# -lt 3; then
    echo "${PROGNAME}: not enough arguments" >&2
    usage >&2
    exit 1
fi

BASE=$1
CURRENT=$2
OTHER=$3
LOCATION=$4

set -e

echo "ansible-vault-merge ${LOCATION}"

# Determine vault id used for the vault
read -r firstline<$LOCATION
IFS=';' read -ra VAULT_HEADER <<< "$firstline"
if [ ${#VAULT_HEADER[@]} = 4 ]; then
	VAULT_ID="${VAULT_HEADER[3]}"
fi

ansible-vault decrypt $BASE > /dev/null
ansible-vault decrypt $CURRENT > /dev/null
ansible-vault decrypt $OTHER > /dev/null

if ! git merge-file -L CURRENT -L BASE -L OTHER $CURRENT $BASE $OTHER; then
    echo "Merge conflict; opening editor to resolve." >&2
    ${EDITOR:-vi} $CURRENT
fi

if [ -z ${VAULT_ID+x} ]; then
	ansible-vault encrypt $CURRENT
else
	ansible-vault encrypt --encrypt-vault-id=$VAULT_ID $CURRENT
fi

