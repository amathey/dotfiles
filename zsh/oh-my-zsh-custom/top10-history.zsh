# Print top 10 commands in history
top10() {
	history | awk '{print $2;}' | sort | uniq -c | sort -rn | head -10
}

