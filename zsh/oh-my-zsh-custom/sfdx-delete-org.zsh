sfdx-delete-orgs() {
	if [[ $# < 1  ]]; then
		echo "Usage: sfdx-delete-orgs [org_usernames...]"
		return 1
	fi
	for i in "$@"; do
		sfdx force:org:delete -p -u "$i"
	done
}

