# Establish a tunnel to a C-Radar docker container
tunnel-docker() {
	if [[ $# == 1 ]]; then
		cmd="sudo docker ps --format '{{.Names}}\t\t{{.Ports}}' 2> /dev/null"
		ssh $1 "${cmd}"
		return 0
	fi
	if [[ $# < 3 ]]; then
		echo "Usage: tunnel-docker ssh-host docker-container port [target_port]"
		return 1
	fi
	target_port="$3"
	if [[ $# == 4 ]]; then
		target_port="$4"
	fi
	echo "Preparing tunnel of container $2 (port $3 -> ${target_port}) on server $1"
	cmd="sudo docker inspect -f '{{ .NetworkSettings.IPAddress }}' $2 2> /dev/null"
	ip=`ssh $1 "${cmd}"`
	echo "Found target IP $ip for container $2"
	ssh -C -L ${target_port}:$ip:$3 $1 'echo "Tunneling..." && cat > /dev/null < /dev/stdin'
}
