dockerps() {
	if [[ $# < 2  ]]; then
		echo "Usage: docker-ps environment inventory-group"
		return 1
	fi
	ansible $2 -i /home/alexis/dev/sidetrade/devops/ansible-sm-deployment/inventories/$1/inventory -m shell -a "docker ps" --become
}
