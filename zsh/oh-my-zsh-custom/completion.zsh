autoload bashcompinit
bashcompinit
# Gitlab autocompletion
_gitlab_completions()
{
   if [ "${#COMP_WORDS[@]}" != "2" ]; then
     return
   fi
   COMPREPLY=($(compgen -W "release" "${COMP_WORDS[1]}"))
}
complete -F _gitlab_completions gitlab

# Deployment completion
_deploy_completions()
{
   if [ "${#COMP_WORDS[@]}" = "2" ]; then
       COMPREPLY=($(compgen -W "dev preprod prod" "${COMP_WORDS[1]}"))
   elif [ "${#COMP_WORDS[@]}" = "3" ]; then
       COMPREPLY=($(compgen -W "cr-app cr-importer cr-importer-social cr-wf cr-plugin sm-app sm-menus" "${COMP_WORDS[2]}"))
   fi
}
complete -F _deploy_completions deploy
