# Access to C-Radar containers
alias mongointe-companies='tunnel-docker cr-dev-bdd companies-mongo-crbddalldev01'
alias mongoprod-companies='tunnel-docker cr-prod-mongo companies-mongo-crbddcompprd01'
alias mongointe-signals='tunnel-docker cr-dev-bdd-wf signal-mongo-crbddwfdev01'
alias postgres-prod='ssh -N -L 5433:127.0.0.1:5432 cr-prod-pg'

# Set git to English
alias git='LANG=en_US.UTF-8 git'

# Lauch & access a Cassandra instance in a docker container
alias docker-cassandra='docker run -p 9042:9042 -p 9142:9142 --rm --name cassandra -e CASSANDRA_START_RPC=true cassandra:3.10'
alias docker-cassandra-client='docker exec -it cassandra cqlsh'

# JBM environments scripts
alias integ-env='cd ~/dev/cradar-apps/jbasemanager/companies-app && ./integration-env.sh cr-dev-app' 
alias production-env='cd ~/dev/cradar-apps/jbasemanager/companies-app && ./prod-env.sh' 

# Extra git aliases
alias gct='git checkout -t'
alias glgv='git log | vim -R -'
alias gdd='git difftool'
alias glogfile='git log -p -M --follow --stat --'

# Gollumn wiki
alias gollum='gollum --live-preview --allow-uploads=dir --emoji --css'

# Display memory/swap usage
alias swap-use='sudo smem -k -r --sort=swap | less'

# Edit ssh config file
alias sshconfig='vim ~/.ssh/config'

# Pretty print json (used with a pipe)
alias pretty-python='python -m json.tool'

# Use vscodium instead of vscode
alias code='codium'

# Ansible S&M deployment
# alias deploy-sm-dev='ansible-playbook -i inventories/dev/inventory sm-app-deploy.yml'
# alias deploy-cr-app-dev='ansible-playbook -i inventories/dev/inventory cr-app-deploy.yml'
# alias deploy-cr-wf-dev='ansible-playbook -i inventories/dev/inventory cr-wf-deploy.yml'
# alias deploy-sm-prod='ansible-playbook -i inventories/prod/inventory sm-app-deploy.yml'
# alias deploy-cr-app-prod='ansible-playbook -i inventories/prod/inventory cr-app-deploy.yml'
# alias deploy-cr-wf-prod='ansible-playbook -i inventories/prod/inventory cr-wf-deploy.yml'
