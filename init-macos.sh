#!/bin/bash

xcode-select --install

#############################
######### HOMEBREW ##########
#############################

# Install Homebrew
/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install.sh)"

# Install packages
IFS=$'\n' read -d '' -r -a lines < brew_packages.txt
packages=$(IFS=' ' ; echo "${lines[*]}")
brew install $packages

# Install cask apps
IFS=$'\n' read -d '' -r -a lines < brew_cask_apps.txt
apps=$(IFS=' ' ; echo "${lines[*]}")
brew cask install $apps

