#!/bin/bash

#############################
######### OH-MY-ZSH #########
#############################

# Install oh-my-zsh
sh -c "$(curl -fsSL https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"

# Link dotfiles
ln -nfs $PWD/zsh/.zshrc $HOME/.zshrc
rm -rf $HOME/.oh-my-zsh/custom
ln -nfs $PWD/zsh/oh-my-zsh-custom $HOME/.oh-my-zsh/custom

#############################
########### TMUX ############
#############################

ln -nfs $PWD/tmux/.tmux.conf $HOME/.tmux.conf
mkdir -p ~/.tmux/plugins
git clone https://github.com/tmux-plugins/tpm ~/.tmux/plugins/tpm

#############################
############ GIT ############
#############################

ln -nfs $PWD/git/.gitconfig $HOME/.gitconfig
ln -nfs $PWD/git/.gitignore-global $HOME/.gitignore-global
ln -nfs $PWD/git/ansible-vault-merge.sh $HOME/.ansible-vault-merge.sh

#############################
############ VIM ############
#############################

mkdir -p ~/.vim ~/.vim/backup ~/.vim/swap ~/.vim/undo
ln -nfs $PWD/vim/vimrc $HOME/.vim/vimrc
# Install Vundle
git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim
# Install Vundle plugins
vim +PluginInstall +qall

#############################
############ FZF ############
#############################

# Setup fzf for zsh
/usr/local/opt/fzf/install
