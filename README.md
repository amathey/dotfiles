# My humble dotfiles

## Install basic packages and apps

```sh
# On Mac OS
./init-macos.sh
```

## Copy dotfiles

```sh
./setup.sh
```

## Manual tasks

### Firefox customization

```sh
# First, find your firefox profile folder 
# On linux, usually in ~/.mozzila/firefox/
# On Mac, usually in ~/Library/Application\ Support/Firefox/Profiles/
# Then:
mkdir $PROFILE_FOLDER/chrome
ln -nfs $PWD/firefox/userChrome.css $PROFILE_FOLDER/chrome/userChrome.css
```
